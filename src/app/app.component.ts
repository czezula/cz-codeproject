import { Component, OnInit } from '@angular/core';
import { TemplatesService } from './shared/templates.service';
import {NgxGalleryOptions, NgxGalleryImage} from '@kolkov/ngx-gallery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'cz-codeproject';

  // templates are obtained from the backend call to the tempateService:
  templates: Array<Template> = [];

  // Define template deatils in this component to display when the thumbnail is selected
  // (or when the imageActions icon is selected if I can't easily get the previous statement implemented...)
  imgTitle = '';
  cost = '';
  id = '';
  description = '';
  thumbnail = '';
  image = '';

  galleryOptions: NgxGalleryOptions[] = [];
  galleryImages: NgxGalleryImage[] = [];

  constructor(private templatesService: TemplatesService) { }

  ngOnInit() {
    this.templatesService
      .getTemplates()
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      .then((result: Array<object>) => {
        this.templates = result;

        // Populate galleryImages array with required keys on each object:
        // {
        //    small:  'assets/images/thumbnails/' + template.thumbnail
        //    medium: 'assets/images/large/' + template.image
        // }
        // All other template keys/values will also be spread into galleryImages to display details
        for (const template of this.templates) {
          const thumbnailPrefix = 'assets/images/thumbnails/'
          const thumbnailName = template.thumbnail
          const imagePrefix = 'assets/images/large/'
          const imageName = template.image
          const galleryImage = {
            small: thumbnailPrefix + thumbnailName,
            medium: imagePrefix + imageName,
            ...template
          }
          this.galleryImages.push(galleryImage)
        }
      });

      // See https://github.com/kolkov/ngx-gallery for description of available options
      this.galleryOptions = [
        {
          width: '600px',
          height: '400px',
          arrowPrevIcon: "thumbnails previous",
          arrowNextIcon: "thumbnails next",
          thumbnailsColumns: 4,
          thumbnailsMoveSize: 4,
          imageArrows: false,
          imageActions: [
            {icon: 'fa fa-times-circle', onClick: this.getImageDetails.bind(this), titleText: 'show image details'}
          ],
          preview: false
        },
        // max-width 800
        {
          breakpoint: 800,
          width: '100%',
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20
        },
        // max-width 400
        {
          breakpoint: 400,
          preview: false
        }
      ];
  }

  getImageDetails(_event: Event, index: number): void {
    const template = this.galleryImages[index];
    // TODO: Extend NgxGalleryImage to include additional details below
    // @ts-expect-error ts-migrate(2451) FIXME: Property 'title' does not exist on type 'NgxGalleryImage'
    this.imgTitle = template.title;
    // @ts-expect-error ts-migrate(2451) FIXME: Property 'cost' does not exist on type 'NgxGalleryImage'
    this.cost = template.cost;
    // @ts-expect-error ts-migrate(2451) FIXME: Property 'id' does not exist on type 'NgxGalleryImage'
    this.id = template.id;
    // @ts-expect-error ts-migrate(2451) FIXME: Property 'description' does not exist on type 'NgxGalleryImage'
    this.description = template.description;
    // @ts-expect-error ts-migrate(2451) FIXME: Property 'thumbnail' does not exist on type 'NgxGalleryImage'
    this.thumbnail = template.thumbnail;
    // @ts-expect-error ts-migrate(2451) FIXME: Property 'image' does not exist on type 'NgxGalleryImage'
    this.image = template.image;
  }
}

export type Template = {
  title?: string;
  cost?: string;
  id?: string;
  description?: string;
  thumbnail?: string;
  image?: string;
};
