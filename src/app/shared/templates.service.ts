import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TemplatesService {
  private url = '/api/templates';

  constructor(private http: HttpClient) { }

  // Get the templates
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getTemplates(): Promise<void | any> {
    return this.http.get(this.url)
      .toPromise()
      .catch(this.error);
  }

  // Error handling
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private error (error: any) {
    const message = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(message);
  }
}
