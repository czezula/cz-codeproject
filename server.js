
var express = require("express");
var bodyParser = require("body-parser");
var fs = require("fs-extra");

// Create new instance of the express server
var app = express();

// Define the JSON parser as a default way
// to consume and produce data through the
// exposed APIs
app.use(bodyParser.json());

// Create link to Angular build directory
// The `ng build` command will save the result
// under the `dist` folder.
var distDir = __dirname + "/dist/";
app.use(express.static(distDir));

// Init the server
var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
});

/*  "/api/templates"
 *   GET: Get server templates for displaying images (thumbnails and large)
 */
app.get("/api/templates", function (req, res) {
  var templateData = fs.readJSONSync('./data/templates.json');
  res.status(200).json(templateData);
});
